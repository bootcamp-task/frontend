"use client";
import React, { useState, useEffect } from "react";
import { userService, imageService } from "../axios";

export default function Home() {
  const [users, setUsers] = useState([]);
  const [username, setUsername] = useState("");
  const [images, setImages] = useState([]);
  const [usersError, setUsersError] = useState(null);
  const [imagesError, setImagesError] = useState(null);

  const getUsers = async () => {
    try {
      const { data } = await userService.get("/users");
      setUsers(data);
    } catch (error) {
      console.error(error);
      setUsersError(error.message);
    }
  };
  const getImages = async () => {
    try {
      const { data } = await imageService.get("/files");
      setImages(data.files);
    } catch (error) {
      console.error(error);
      setImagesError(error.message);
    }
  };

  useEffect(() => {
    getUsers();
    getImages();
  }, []);

  const createUser = async () => {
    await userService.post("/user", { username });
    setUsername("");
    await getUsers();
  };

  const deleteUser = async (id) => {
    await userService.delete(`/user/${id}`);
    await getUsers();
  };

  const uploadImage = async (file) => {
    const formData = new FormData();
    formData.append("file", file);
    await imageService.post("/upload", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    await getImages();
  };
  return (
    <div className="container mx-auto px-4">
      <h2 className="text-2xl font-bold mb-4">Create User</h2>
      <div className="mb-8">
        <input
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          className="border px-4 py-2 mr-4"
        />
        <button
          onClick={createUser}
          className="bg-blue-500 text-white px-4 py-2 rounded"
        >
          Create User
        </button>
      </div>
      <h2 className="text-2xl font-bold mb-4">All Users</h2>
      {usersError && <div className="text-red-500 mb-4">{usersError}</div>}
      {users.map((user) => (
        <div key={user.id} className="mb-4">
          <span className="mr-4">{user.username}</span>
          <button
            onClick={() => deleteUser(user.id)}
            className="bg-red-500 text-white px-4 py-2 rounded"
          >
            Delete
          </button>
        </div>
      ))}

      <h2 className="text-2xl font-bold mb-4">Upload Image</h2>
      <div className="mb-8">
        <input
          type="file"
          onChange={(e) => uploadImage(e.target.files[0])}
          className="border px-4 py-2 mr-4"
        />
      </div>

      <h2 className="text-2xl font-bold mb-4">All Images</h2>
      {imagesError && <div className="text-red-500 mb-4">{imagesError}</div>}
      {images.map((image, index) => (
        <div key={index} className="mb-4">
          <img src={image.url} alt={image.file} className="w-64" />
        </div>
      ))}
    </div>
  );
}
