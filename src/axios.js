import axios from "axios";

// if process.env.NEXT_PUBLIC_USERS_API is empty, log warning
if (!process.env.NEXT_PUBLIC_USERS_API) {
  console.warn("The env variable NEXT_PUBLIC_USERS_API is not defined.");
}

// if process.env.NEXT_PUBLIC_IMAGES_API is empty, log warning
if (!process.env.NEXT_PUBLIC_IMAGES_API) {
  console.warn("The env variable NEXT_PUBLIC_IMAGES_API is not defined.");
}

export const userService = axios.create({
  baseURL: process.env.NEXT_PUBLIC_USERS_API, // Replace with your API end point
});

export const imageService = axios.create({
  baseURL: process.env.NEXT_PUBLIC_IMAGES_API, // Replace with your API end point
});
